package mvn_calc.mvn_calc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
	@Test
	public void checkForNoInput() {
		
		
		Calculator test = new Calculator();
        
        assertEquals(0,test.Add(""));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForOneNumber() {
		
		
		Calculator test = new Calculator();
        
        assertEquals(10,test.Add("10"));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForTwoNumbers() {
		
		
		Calculator test = new Calculator();
        
        assertEquals(3,test.Add("1,2"));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForUnkownAmoutOfNumber() {
		
		
		Calculator test = new Calculator();
        
        assertEquals(6,test.Add("1,2,3"));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForNewLines() {
		
		Calculator test = new Calculator();
        
        assertEquals(8,test.Add("1\n2,5"));

	}
	
	@Test
	public void checkSpecialDelimiter() {
		
		Calculator test = new Calculator();
        
        assertEquals(5,test.Add("//;\n2;3"));

	}
	
	@Test(expected=IllegalArgumentException.class)
	public void checkForNegativeNumberThrowException() {
		
		Calculator test = new Calculator();
        
        assertEquals(5,test.Add("//;\n-2;-3"));

	}
  
}
